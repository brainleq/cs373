# -----------
# Wed, 26 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

7812

# 1. map as a class

m = map(...)
v = next(m)  # m.__next__()
p = iter(m)  # m.__iter__()

class map :
    def __init__ (self, uf, a) :
        ...

    def __next__ (self) :
        ...

    def __iter__ (self) :
        ...

# 2. map as a function with yield

def map (uf, a) :
    ...yield...

# 3. map as a function with a generator comprehension

def map (uf, a) :
    ...(...)...
































# 7812

# 1. map as a class

m = map(...)
v = next(m)  # m.__next__()
p = iter(m)  # m.__iter__()

class map :
    def __init__ (self, uf, a) :
        ...

    def __next__ (self) :
        ...

    def __iter__ (self) :
        ...

# 2. map as a function as a function with yield

def map (uf, a) :
    ...yield...

# 3. map as a function without yield, with a generator comprehension

def map (uf, a)
    ...(...)...































