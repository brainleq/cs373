# -----------
# Mon, 24 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
a generator is an iterator
not all iterators are generators
"""

"""
map takes two arguments
    a unary function
    an iterable
"""

"""
filter takes two arguments
    a unary predicate (a function that returns a boolean)
    an iterable
"""

def f () :
    print("abc")
    yield(2)
    print("def")
    print("ghi")
    yield(3)
    print("jkl")

p = f()        # nothing print, f does NOT run
print(type(p)) # generator

v = next(p)    # abc
print(v)       # 2

v = next(p)    # def ghi
print(v)       # 3

v = next(p)    # jkl, raises StopIteration
print(v)       # 3
