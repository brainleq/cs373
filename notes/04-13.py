# -----------
# Mon, 13 Apr
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
multiple wildcard

SQL:   %
Pyton: .*
"""

"""
single wildcard

SQL:    _
Python: .
"""
