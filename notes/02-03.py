# -----------
# Mon,  3 Feb
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
assertions are not good for user errors
exceptions are good
"""

"""
let's pretend there are no exceptions, like C
"""

# use the return

def f (...) :
    ...
    if (<something wrong>)
        return <special val>
    ...

def g (...) :
    ...
    x = f(...)
    if (x == <special val>) :
        <something wrong>
    ...

# use a global

h = 0

def f (...) :
    global h
    ...
    if (<something wrong>)
        h = <special val>
        return ...
    ...

def g (...) :
    global h
    ...
    h = 0
    x = f(...)
    if (h == <special val>) :
        <something wrong>
    ...

# use a parameter, doesn't work because of pass by value

def f (..., e2) :
    ...
    if (<something wrong>)
        e2 = <special val>
        return ...
    ...

def g (...) :
    ...
    e = 0
    x = f(..., e)
    if (e == <special val>) :
        <something wrong>
    ...

# Java: use a parameter, doesn't work because of pass by value

class Test {
    void f (..., int e2) :
        ...
        if (<something wrong>)
            e2 = <special val>
            return ...
        ...

    void g (...) :
        ...
        int e = 0
        int x = f(..., e)
        if (e == <special val>) :
            <something wrong>
        ...

# Java: use a parameter, doesn't work because of Integer being immutable

Integer x = new Integer(0);
Integer y = 0;              # ok, autoboxing

class Test {
    void f (..., Integer e2) :
        ...
        if (<something wrong>)
            e2 = <special val>
            return ...
        ...

    void g (...) :
        ...
        Integer e = 0
        int x = f(..., e)
        if (e == <special val>) :
            <something wrong>
        ...

# use a parameter with a list

def f (..., e2) :
    ...
    if (<something wrong>)
        e2[0] = <special val>
        return ...
    ...

def g (...) :
    ...
    e = [0]
    x = f(..., e)
    if (e[0] == <special val>) :
        <something wrong>
    ...

# use an exception

def f (...) :
    ...
    if (<something wrong>)
        raise E()          # same as raise E
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except E as e :
        <something wrong>
    else :
        # only runs if no exception was raised
    finally :
        # always runs
    ...

...
g(...)
...

# use an exception

def f (...) :
    ...
    if (<something wrong>)
        raise E2()
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except E as e :
        <something wrong>
    else :                # only runs if no exception was raised
		...
    finally :             # always run
        ...
    ...                   # doesn't run

...
g(...)
...

# use an exception

def f (...) :
    ...
    if (<something wrong>)
        raise Tiger()
    ...

def g (...) :
    ...
    try :
        ...
        x = f(...)
        ...
    except Mammal as e :
        <something wrong>
    except Tiger as e :   # handle children first!
        <something wrong>
    else :                # only runs if no exception was raised
		...
    finally :             # always run
        ...
    ...

...
g(...)
...

a = [2, 3, 4]
b = [2, 3, 4]
print(a == b) # true;  in Java .equals()
print(a is b) # false; in Java ==

a = [2, 3, 4]
b = a         # O(1)
print(a is b) # true

a = [2, 3, 4]
print(type(a)) # list
a = []
print(type(a)) # list
a = [2]
print(type(a)) # list

a = (2, 3, 4)
print(type(a)) # tuple
a = ()
print(type(a)) # tuple
a = (2)
print(type(a)) # int
a = (2,)
print(type(a)) # tuple

a = []
b = []
print(a is b) # false

a = ()
b = ()
print(a is b) # true
