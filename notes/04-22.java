// -----------
// Wee, 22 Apr
// -----------

/*
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
*/

/*
Alvin: 1, 2, 3
Glenn: 4, 5, 6, 7
Chenzi, 8, 9, 10
*/

/*
Abhinash: 1, 2, 3
Glenn: 4, 5, 6, 7
Hannah: 8, 9, 10
*/

/*
inheritance?

Movie
    RegularMovie
    NewMovie
    ChildrensMovie

problems
    would have to change the tests
    hard to change movie type
*/

/*
containment?

Price
    RegularPrice
    NewPrice
    ChildrensPrice

Movie
    Price
*/


/*
containmemt?

Price
    RegularPrice
    NewPrice
    ChildrensPrice

Movie
    Price
*/
