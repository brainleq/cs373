# -----------
# Wed, 29 Jan
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,	 3-4pm,		Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
	ask and answer questions
	please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
if n is odd, 3n + 1
(3n + 1) / 2
3n/2 + 1/2
n + n/2 + 1/2
n is odd, n/2 loses a 1/2
n + n/2 + 1
"""

"""
in this case, we're expected to read, until that fails
a second option would be to tell me up front
a third option would be to use a sentinel at the end
"""

"""
write the simplest program first
"""

"""
how to speed it up
let's build a cache (an array)
how big should we make the array?
let's try 1,000,000
experiment with smaller arrays
when caching also remember intermediate cycle lengths
this is a lazy cache
"""

"""
an eager cache computes and stores cycle lengths BEFORE reading
how big should we make the array?
let's try 1,000,000
experiment with smaller arrays
"""

"""
a meta cache computes and stores cycle lengths OUTSIDE of HackerRank
how big should we make the array?
let's try 1,000,000
doesn't work
HackerRank has a source file limit of 50k
what if I cached NOT cycle lengths, but MAX cycle lengths for a range

1, 1000
1000, 2000
2000, 3000
3000, 4000
...
999900 1000000

500, 2500
compute 500 to 1000
lookup 1000 to 2000
compute 2000 to 2500
"""
