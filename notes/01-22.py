# -----------
# Wed, 22 Jan
# -----------

"""
Cold Calling
	you're only called ONCE per rotation
	it's totally fine to be wrong, the idea is to discuss and to learn

FoCS
	please track and go
	really worth your time

Lab Sessions
	Tue, 6-8pm, GDC 1.302
	Thu, 6-8pm, GDC 1.302

Office Hours
	Glenn
		MF, 12-12:45pm, GDC 6.302
		T,   3-4pm,     Zoom, https://us04web.zoom.us/j/412178924

	Chenxi
		F, 2:30-3:30pm, GDC 1.302

	Hannah
		M, 2-3pm, GDC 1.302

	Abhinav
		W, 2-3pm, GDC 1.302

	Alvin
		T, 1-2pm, GDC 1.302

Canvas
	personal questions

Piazza
	class questions
    ask and answer questions
    please be proactive

Notes
	these notes are on GitLab and Piazza
"""

"""
please no devices except for exercises and quizzes
"""

"""
competitive programming

ACM, 80 years old
ICPC, 40 years old, divorce from ACM last year

350 regions in the world

our region, SCUSA, 65 teams, 25 schools, 3 states
hosted at LSU
we actually go to Baylor
we go to Baylor with 4 teams, 12 students

if your team wins the region you go to the world finals

2014: 2nd; Rice, 1st, Morocco
2015: 2nd; Rice, 1st, Thailand; our 4 teams were all in the top 7 of the 65
2016: 1st, 2nd, 3rd, 4th, South Dakota
2017: 1st, Beijing
2018: 1st, Portugal
2019: 1st, Moscow in Jun
"""

"""
get to know each other
exchange some contact info
why are you taking the class, why now
what are your expectations of the class
"""

"""
google "cs373 fall 2019 final entry"
"""
