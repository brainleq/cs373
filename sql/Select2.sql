-- -----------
-- Select2.sql
-- -----------

use test;

-- -----------------------------------------------------------------------
select "*** create table Student ***";
create table Student (
    sID         int not null,
    sName       text,
    GPA         float,
    sizeHS      int,
    primary key (sID))
    engine = innodb;

-- -----------------------------------------------------------------------
select "*** create table College ***";
create table College (
    cName       varchar(8) not null,
    state       char(2),
    enrollment  int,
    primary key (cName))
    engine = innodb;

-- -----------------------------------------------------------------------
select "*** create table Apply ***";
create table Apply (
    sID         int,
    cName       varchar(8),
    major       text,
    decision    boolean,
    foreign key (sID)   references Student (sID),
    foreign key (cName) references College (cName))
    engine = innodb;

-- -----------------------------------------------------------------------
-- student ID, school name, and major of
-- applications that were accepted
-- sorted by school in ascending order

select "*** query #1 ***";

-- -----------------------------------------------------------------------
-- distinct school name and decision of
-- applications to CS that were accepted
-- sorted by school in descending order
-- limited to two results

select "*** query #2 ***";

-- -----------------------------------------------------------------------
-- student name and high school size of
-- students whose names end in "y"

select "*** query #3 ***";

-- -----------------------------------------------------------------------
-- min, max, and average high school size of
-- students whose names have three letters and end in "y"

select "*** query #4 ***";

-- -----------------------------------------------------------------------
-- number of schools in CA or TX
-- MUST USE in

select "*** query #5 ***";

-- -----------------------------------------------------------------------
-- state, min, max, and avg enrollment of
-- schools whose enrollment is between 20000 and 30000
-- for each state
-- MUST USE between

select "*** query #6 ***";

-- -----------------------------------------------------------------------
-- state, min, max, and average enrollment of
-- schools whose min enrollment is between 15000 and 25000
-- for each state
-- MUST USE having

select "*** query #7 ***";

exit
